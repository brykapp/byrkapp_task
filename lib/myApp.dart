import 'package:byrapp_task/pages/SqlFlite/Graphql/graphqlAddUser.dart';
import 'package:flutter/material.dart';

// Import Pages
import 'package:byrapp_task/pages/fetchingDataJsonPage.dart';
import 'package:byrapp_task/pages/fetchingDataLocalStoragePage.dart';
import 'package:byrapp_task/pages/SqlFlite/addUsersPage.dart';



class MyApp extends StatefulWidget {
 
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }

}



class MyAppState extends State<MyApp> {

  int _currentIndex = 0;
  final List<Widget> _children = [
    FetchDataJsonPage(),
    FetchLocalStoragePage(),
    AddUsersPage(),
    GraphqlAddUser()
  ];
 

  @override
  Widget build(BuildContext context) {
      return Scaffold(


        
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.person), title: Text('Json')),
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('LocalStorage')),
            BottomNavigationBarItem(
                icon: Icon(Icons.dashboard), title: Text('Sql')),
                BottomNavigationBarItem(
                icon: Icon(Icons.graphic_eq), title: Text('Graphql')),
          
          ],
        ),
      );
    
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
