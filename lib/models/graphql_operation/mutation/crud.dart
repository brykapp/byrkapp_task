// Create User
const  String  create = """
  mutation createUser(
    \$name: String!
    \$password:String!
  ){
  createUser(params:{
    name:\$name
    password:\$password
    }){
    id
  }
}
""";



// Update User
const String updateUser = '''
mutation updateUser(
  \$id: String!
  \$name: String!
  \$password: String!
){
  updateUser(id: \$id, params:{
    name: \$name
    password: \$password
  }){
    name
    
  }
}
''';

// Delete User
const  String  deleteUser = """
  mutation deleteUser(
    \$id: String!
  ){
  deleteUser(id:\$id){
    id
  }
}
""";

