import 'package:byrapp_task/models/Database_helper.dart';
import 'package:byrapp_task/pages/SqlFlite/UsersPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AddUsersPage extends StatefulWidget {
  @override
  _AddUsersPageState createState() => _AddUsersPageState();
}

class _AddUsersPageState extends State<AddUsersPage> {
///////////////////////////////
//////////////////////////////  Text Controller  ///////////////////////////////////////
/////////////////////////////
  String columnName = '';
  String columnAge = '';
  var userData;

  final TextEditingController _columnNameController = TextEditingController();
  final TextEditingController _columnAgeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _columnNameController.addListener(_updateColmnName);
    _columnAgeController.addListener(_updateColmnAge);
  }

  @override
  dispose() {
    _columnNameController.removeListener(_updateColmnName);
    _columnNameController.dispose();
    _columnAgeController.removeListener(_updateColmnAge());
    _columnAgeController.dispose();
    super.dispose();
  }

  _updateColmnName() => setState(() => columnName = _columnNameController.text);
  _updateColmnAge() => setState(() => columnAge = _columnAgeController.text);

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

  // H1 Style
  final titleStyle = new TextStyle(
      fontSize: 27.0, fontWeight: FontWeight.bold, color: Colors.black);

  // reference to our single class that manages the UserDatabase
  final dbHelper = DatabaseHelper.instance;

  // homepage layout
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('sqflite'),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text('Add User to SqlFlite', style: titleStyle),
                  padding: const EdgeInsets.all(20.0),
                ),
                TextField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(2.0)),
                      prefixIcon: Icon(Icons.lock),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 10.0),
                      labelText: 'Name'),
                  controller: _columnNameController,
                  onChanged: (String columnName) {
                    setState(() {
                      columnName = columnName;
                    });
                  },
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(2.0)),
                      prefixIcon: Icon(Icons.lock),
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 10.0),
                      labelText: 'Age'),
                  keyboardType: TextInputType.number,
                  controller: _columnAgeController,
                  onChanged: (String columnAge) {
                    setState(() {
                      columnAge = columnAge;
                    });
                  },
                ),
                RaisedButton(
                  child: Text('ADD'),
                  onPressed: () {
                    _insert();
                  },
                ),
                RaisedButton(
                  child: Text('Users List'),
                  onPressed: () {
                    Navigator.push<bool>(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) =>
                              UsersPage(userData),
                        ));
                  },
                ),
              ],
            ),
          ),
        ));
  }

  // Button onPressed methods

  void _insert() async {
    // row to insert
    Map<String, dynamic> row = {
      DatabaseHelper.columnName: columnName,
      DatabaseHelper.columnAge: columnAge
    };
    final id = await dbHelper.insert(row);
    print('inserted row id: $id');
    print(row);
  }
}
