import 'dart:async';

import 'package:byrapp_task/models/database_helper.dart';
import 'package:byrapp_task/pages/SqlFlite/deleteUserPage.dart';
import 'package:flutter/material.dart';

class UpdateUserPage extends StatefulWidget {
  final userData;
  UpdateUserPage(this.userData);

  @override
  _UpdateUserPageState createState() => _UpdateUserPageState(userData);
}

class _UpdateUserPageState extends State<UpdateUserPage> {
  final userData;
  _UpdateUserPageState(this.userData);

  // reference to our single class that manages the UserDatabase
  final dbHelper = DatabaseHelper.instance;

///////////////////////////////
//////////////////////////////  Text Controller  ///////////////////////////////////////
/////////////////////////////
  String columnName = '';
  int columnAge;

  final TextEditingController _columnNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _columnNameController.addListener(_updateColmnName);
  }

  @override
  dispose() {
    _columnNameController.removeListener(_updateColmnName);
    _columnNameController.dispose();
    super.dispose();
  }

  _updateColmnName() => setState(() => columnName = _columnNameController.text);

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          print('Back button pressed!');
          Navigator.pop(context, false);
          return Future.value(false);
        },
        child: Scaffold(
            appBar: AppBar(
              title: Text(userData['name']),
            ),
            body: SingleChildScrollView(
                child: Stack(
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 50, 0, 0),
                  child: TextField(
                    controller: _columnNameController,
                    onChanged: (String columnName) {
                      setState(() {
                        columnName = columnName;
                      });
                    },
                    decoration: InputDecoration(hintText: 'Name'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 150, 0, 0),
                  child: TextField(
                    onChanged: (input) {
                      print(input);
                      int ageFin = int.parse(input);
                      assert(ageFin is int);
                      setState(() {
                        columnAge = ageFin;
                      });
                    },
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(hintText: 'Age'),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 400, 0, 0),
                      child: Center(
                          child: RaisedButton(
                              color: Theme.of(context).accentColor,
                              child: Text('Confirm Update'),
                              onPressed: () async {
                                _update();
                              })),
                    )
                  ],
                ),
                Column(
                  // Padd: EdgeInsets.all(10.0),
                  children: <Widget>[
                    // Padding:EdgeInsets.all(10.0)
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 450, 0, 0),
                      child: Center(
                        child: RaisedButton(
                          color: Colors.red,
                          child: Text('DELETE'),
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    DeleteUserPage(userData),
                              )),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ))));
  }

  void _update() async {
    // row to update
    Map<String, dynamic> row = {
      DatabaseHelper.columnId: userData['_id'],
      DatabaseHelper.columnName: columnName,
      DatabaseHelper.columnAge: columnAge
    };
    print(row);
    print(userData['id']);
    print(columnName);
    print(columnAge);

    final rowsAffected = await dbHelper.update(row);
    print('updated $rowsAffected row(s)');
  }
}
