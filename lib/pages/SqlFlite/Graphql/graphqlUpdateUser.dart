import 'package:byrapp_task/myApp.dart';
import 'package:byrapp_task/pages/SqlFlite/Graphql/graphqlDeleteUser.dart';
import 'package:flutter/material.dart';

// Graphql Package
import 'package:graphql_flutter/graphql_flutter.dart';

// mutation
import '../../../models/graphql_operation/mutation/mutationsExport.dart'
    as mutation;

//port
import '../../../models/graphql_operation/port.dart';

class GraphqlUpdateUser extends StatelessWidget {
  final data;
  GraphqlUpdateUser(this.data);

  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink = HttpLink(uri: port);
    final ValueNotifier<GraphQLClient> client =
        ValueNotifier<GraphQLClient>(GraphQLClient(
      link: httpLink ,
      cache:
          NormalizedInMemoryCache(dataIdFromObject: typenameDataIdFromObject),
    ));
    return GraphQLProvider(
      child: UserUpdatePg(data),
      client: client,
    );
  }
}

class UserUpdatePg extends StatefulWidget {
  final data;
  UserUpdatePg(this.data);

  @override
  _UserUpdatePgState createState() => _UserUpdatePgState(data);
}

class _UserUpdatePgState extends State<UserUpdatePg> {
  final data;
  _UserUpdatePgState(this.data);

  String name;
  String password;
  

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  _updateName() => setState(() => name = _nameController.text);
  _updatePassword() => setState(() => password = _passwordController.text);

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  void initState() {
    super.initState();
    _nameController.addListener(_updateName);
    _passwordController.addListener(_updatePassword);
  }

  @override
  dispose() {
    //name
    _nameController.removeListener(_updateName);
    _nameController.dispose();
    //password
    _passwordController.removeListener(_updatePassword);
    _passwordController.dispose();
   

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print('Back button pressed!');
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(data['name']),
        ),
        body: Mutation(
          options: MutationOptions(document: mutation.updateUser),
          builder: (RunMutation update, result) {
            final userId = data['id'];
            updateUs() async {

              update({
                "id": userId,
                "name": name,
                "password": password,
                
              });
            }
                

            return 
            SingleChildScrollView(
              child: 
            
            Stack(
              // crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 50, 0, 0),
                  child: TextField(
                    controller: _nameController,
                    decoration: InputDecoration(hintText: 'name'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 100, 0, 0),
                  child: TextField(
                    controller: _passwordController,
                    decoration: InputDecoration(hintText: 'password'),
                  ),
                ),
               
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 400, 0, 0),
                      child: Center(
                        child: RaisedButton(
                            color: Theme.of(context).accentColor,
                            child: Text('Confirm Update'),
                            onPressed: () async {
                              await updateUs();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) => MyApp(),
                                  ));
                            }),
                      ),
                    )
                  ],
                ),
                Column(
                  // Padd: EdgeInsets.all(10.0),
                  children: <Widget>[
                    // Padding:EdgeInsets.all(10.0)
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 450, 0, 0),
                      child: Center(
                        child: RaisedButton(
                          color: Colors.red,
                          child: Text('DELETE'),
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    GraphqlDeleteUser(data),
                              )),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ));
          },
        ),
      ),
    );
  }
}
