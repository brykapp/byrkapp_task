import 'dart:async';
import 'package:byrapp_task/pages/SqlFlite/Graphql/graphqlUpdateUser.dart';
import 'package:flutter/material.dart';

class GraphqlUserDetails extends StatefulWidget {
  final data;
  GraphqlUserDetails(this.data);

  @override
  _GraphqlUserDetailsState createState() => _GraphqlUserDetailsState(data);
}

class _GraphqlUserDetailsState extends State<GraphqlUserDetails> {
  final data;
  _GraphqlUserDetailsState(this.data);

  final fontStaticStyle = new TextStyle(
      fontSize: 22.0, fontWeight: FontWeight.bold, color: Colors.black);
  final fontDataStyle = new TextStyle(fontSize: 20.0, color: Colors.blueAccent);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          print('Back button pressed!');
          Navigator.pop(context, false);
          return Future.value(false);
        },
        child: Scaffold(
            appBar: AppBar(
              title: Text(data['name']),
            ),
            body: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    data['name'],
                    style: fontStaticStyle,
                  ),
                ),
                Center(
                  child: Text(
                    data['password'],
                    style: fontDataStyle,
                  ),
                ),
                RaisedButton(
                  child: Text('Edit'),
                  onPressed: () {
                    Navigator.push<bool>(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) =>
                              GraphqlUpdateUser(data),
                        ));
                  },
                )
              ],
            )));
  }
}
