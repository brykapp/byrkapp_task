import 'package:flutter/material.dart';

// Graphql Package
import 'package:graphql_flutter/graphql_flutter.dart';

// mutation
import '../../../models/graphql_operation/query/queriesExport.dart'
    as query;

//port
import '../../../models/graphql_operation//port.dart';
import 'graphqlUserDetails.dart';


class GraphqlUsersList extends StatefulWidget {
  @override
  _StudentsPgState createState() => _StudentsPgState();
}

class _StudentsPgState extends State<GraphqlUsersList> {
 

  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink =
        HttpLink(uri: port);
    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
        GraphQLClient(link: httpLink , cache: InMemoryCache()));
    return GraphQLProvider(
        client: client,
        child: new Scaffold(

         
          body: Stack(
            children: <Widget>[




              Padding(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Query(
                  options: QueryOptions(
                    document: query.fetchUsers,
                  ),
                  builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
                    if (result.data == null) {
                      return Center(child: CircularProgressIndicator());
                    }
                    if (result.loading) {
                      print('could not fetch');
                      return new Center(child: CircularProgressIndicator());
                    }

                    return ListView.builder(
                      itemBuilder: (BuildContext context, int index) {
                        final data = (result.data['fetchUsers'][index]);
                        return Padding(
                          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                          child: Card(
                              elevation: 0.8,
                              child: ListTile(
                                contentPadding: EdgeInsets.all(5),
                                title: Text(data['name']),
                                dense: true,
                                trailing: Icon(Icons.chevron_right),
                               
                                onTap: () {
                                  Navigator.push<bool>(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            GraphqlUserDetails(data),
                                      ));
                                },
                              )),
                        );
                      },
                      itemCount: result.data['fetchUsers'].length,
                    );
                  },
                ),
              )
            ],
          ),
        ));
  }
}
