import 'package:byrapp_task/pages/SqlFlite/Graphql/graphqlUsersList.dart';
import 'package:flutter/material.dart';

// Graphql Package
import 'package:graphql_flutter/graphql_flutter.dart';

// mutation
import '../../../models/graphql_operation/mutation/mutationsExport.dart'
    as mutation;

//port
import '../../../models/graphql_operation/port.dart';

class GraphqlAddUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink = HttpLink(uri: port);
    final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
      GraphQLClient(
        link: httpLink,
        cache: InMemoryCache(),
      ),
    );

    return GraphQLProvider(
      child: AddUser(),
      client: client,
    );
  }
}

class AddUser extends StatefulWidget {
  @override
  _AddUserState createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
///////////////////////////////
//////////////////////////////  Text Controller  ///////////////////////////////////////
/////////////////////////////
  String name;
  String password;

  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _nameController.addListener(_updateName);
    _passwordController.addListener(_updatePassword);
  }

  @override
  dispose() {
    _nameController.removeListener(_updateName);
    _nameController.dispose();
    _passwordController.removeListener(_updatePassword);
    _passwordController.dispose();
    super.dispose();
  }

  _updateName() => setState(() => name = _nameController.text);
  _updatePassword() => setState(() => password = _passwordController.text);

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Graphql'),
      ),
      body: Mutation(
        options: MutationOptions(document: mutation.create),
        builder: (RunMutation createUs, result) {
          userCreate() async {
            createUs({"name": name, "password": password});
          }

          return Stack(children: <Widget>[
            Align(
                alignment: Alignment.bottomCenter,
                child: ListView(
                  shrinkWrap: true,
                  primary: true,
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  // mainAxisAlignment: MainAxisAlignment.center,

                  children: <Widget>[
                    SizedBox(
                      height: 48.0,
                    ),
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        prefixIcon: Icon(Icons.person),
                        labelText: 'Name',
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 10.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                      ),
                      controller: _nameController,
                      onChanged: (String name) {
                        setState(() {
                          name = name;
                        });
                      },
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                    TextField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0)),
                          prefixIcon: Icon(Icons.lock),
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 10.0),
                          labelText: 'Password'),
                      controller: _passwordController,
                      // obscureText: true,
                      onChanged: (String password) {
                        setState(() {
                          password = password;
                        });
                      },
                    ),
                    SizedBox(
                      height: 0.0,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0),
                        ),
                        onPressed: () async {
                          await userCreate();
                        },
                        padding: EdgeInsets.all(12),
                        color: Colors.lightBlueAccent,
                        child: Text('Create user',
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0),
                      ),
                      onPressed: () async {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  GraphqlUsersList(),
                            ));
                      },
                      padding: EdgeInsets.all(12),
                      color: Colors.lightBlueAccent,
                      child: Text('Users List',
                          style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ))
          ]);
        },
      ),
    );
  }
}
