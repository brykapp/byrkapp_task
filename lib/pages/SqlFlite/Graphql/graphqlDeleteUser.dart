import 'package:byrapp_task/myApp.dart';
import 'package:flutter/material.dart';

// Graphql Package
import 'package:graphql_flutter/graphql_flutter.dart';

// mutation
import '../../../models/graphql_operation/mutation/mutationsExport.dart'
    as mutation;

//port
import '../../../models/graphql_operation/port.dart';


class GraphqlDeleteUser extends StatelessWidget {
  final data;
  GraphqlDeleteUser(this.data);
  @override
  Widget build(BuildContext context) {
    final HttpLink httpLink = HttpLink(uri: port);
    final ValueNotifier<GraphQLClient> client =
        ValueNotifier<GraphQLClient>(GraphQLClient(
            link: httpLink  ,
            cache: OptimisticCache(
              dataIdFromObject: typenameDataIdFromObject,
            )));

    return GraphQLProvider(
      child: GraphqlDeleteAlert(data),
      client: client,
    );
  }
}

class GraphqlDeleteAlert extends StatefulWidget {
  final data;

  GraphqlDeleteAlert(this.data);

  @override
  _GraphqlDeleteAlertState createState() => _GraphqlDeleteAlertState(data);
}

class _GraphqlDeleteAlertState extends State<GraphqlDeleteAlert> {
  final data;

  _GraphqlDeleteAlertState(this.data);



  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          print('Back button pressed!');
          Navigator.pop(context, false);
          return Future.value(false);
        },
        child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              title: Text('delete ' + ' ' + data['name']),
            ),
            body: Mutation(
                options: MutationOptions(document: mutation.deleteUser),
                builder: (RunMutation delete, result) {
                  deleteUser() async {
                    delete({"id": data['id']});
                  }

                  return AlertDialog(
                    title:
                        new Text("Alert ", style: TextStyle(color: Colors.red)),
                    content: new Text("Are you Sure ?"),
                    actions: <Widget>[
                      // usually buttons at the bottom of the dialog
                      new FlatButton(
                        child: new Text('NO'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      new FlatButton(
                          child: new Text("Yes"),
                          onPressed: () async {
                            await deleteUser();
                            await Future.delayed(const Duration(seconds: 2));

                            Navigator.push<bool>(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => MyApp(),
                                ));
                          })
                    ],
                  );
                })));
  }
}
