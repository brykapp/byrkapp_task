import 'dart:async';
import 'package:byrapp_task/models/database_helper.dart';
import 'package:flutter/material.dart';

import '../../myApp.dart';





class DeleteUserPage extends StatefulWidget {
  final userData;

  DeleteUserPage(this.userData);

  @override
  _DeleteUserPageState createState() => _DeleteUserPageState(userData);
}

class _DeleteUserPageState extends State<DeleteUserPage> {
  final userData;

  _DeleteUserPageState(this.userData);

  // reference to our single class that manages the UserDatabase
  final dbHelper = DatabaseHelper.instance;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          print('Back button pressed!');
          Navigator.pop(context, false);
          return Future.value(false);
        },
        child: Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              title: Text('delete ' + ' ' + userData['name']),
            ),
            body:  AlertDialog(
                    title:
                        new Text("Alert ", style: TextStyle(color: Colors.red)),
                    content: new Text("Are you Sure ?"),
                    actions: <Widget>[
                      // usually buttons at the bottom of the dialog
                      new FlatButton(
                        child: new Text('NO'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      new FlatButton(
                          child: new Text("Yes"),
                          onPressed: () async {
                             _delete();
                            await Future.delayed(const Duration(seconds: 2));

                            Navigator.push<bool>(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => MyApp(),
                                ));
                          })
                    ],
            )));
                  
  }
  void _delete() async {
    // Assuming that the number of rows is the id for the last row.
    final id = await dbHelper.queryRowCount();
    final rowsDeleted = await dbHelper.delete(userData['_id']);
    print('deleted $rowsDeleted row(s): row $id');
  }
  }

