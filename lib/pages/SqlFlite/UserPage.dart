import 'dart:async';

import 'package:flutter/material.dart';

import 'UpdateUserPage.dart';

class UserPage extends StatelessWidget {
  final userData;

  UserPage(this.userData);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        print('Back button pressed!');
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text(userData['name']),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              // padding: EdgeInsets.all(10.0),
              child: Text(userData['name']),
            ),
            Container(
              // padding: EdgeInsets.all(10.0),
              child: Text(userData['age'].toString()),
            ),
            Column(
              // Padd: EdgeInsets.all(10.0),
              children: <Widget>[
                // Padding:EdgeInsets.all(10.0)
              Padding(
                padding: EdgeInsets.all(8.0),
              ),
               RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('Edit'),
                onPressed: (){
                   Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  UpdateUserPage(userData),
                            ));
                },
              ),
             
              ],
            )
          ],
        ),
      ),
    );
  }
}
