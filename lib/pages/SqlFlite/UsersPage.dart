import 'package:byrapp_task/models/database_helper.dart';
import 'package:flutter/material.dart';

import 'UserPage.dart';

class UsersPage extends StatefulWidget {
  UsersPage(userData);


  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  List data;

  // H1 Style
  final titleStyle = new TextStyle(
      fontSize: 27.0, fontWeight: FontWeight.bold, color: Colors.black);

  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;

  Future<String> getJsonData() async {
    final allRows = await dbHelper.queryAllRows();
    allRows.map((title) => print(title));

    setState(() {
      data = allRows;
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    getJsonData();
  }

  // homepage layout
  @override
  Widget build(BuildContext context) {
    print(data);
    print('ow');
    return Scaffold(
        appBar: AppBar(
          title: Text('SqlFlite users '),
        ),
        body: Center(
            child: Container(
                child: ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index) {
            final userData = data[index];
            return Container(
              child: Column(
                children: <Widget>[
                  new Card(
                    child: ListTile(
                      title: new Text(data[index]['name']),
                       onTap: () {
                  Navigator.push<bool>(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => UserPage(userData),
                    )
                  );
                },

                      // padding: const EdgeInsets.all(20.0),
                    ),
                  )
                ],
              ),
            );
          },
        ))));
  }

}
