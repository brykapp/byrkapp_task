import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Class Model
import 'package:byrapp_task/models/post.dart';

// Fetch Data
import 'package:http/http.dart' as http;

_saveData(List<dynamic> data) async {
  final prefs = await SharedPreferences.getInstance();
  var allData = jsonEncode(data);
  await prefs.setString('ListOfData', allData);
}

// Save List of data using SharedPreferences
// _save(List<String>data) async {
//   // List<String> myListOfStrings = posts.map((i) => i.toString()).toList();

//   final prefs = await SharedPreferences.getInstance();
//   // List<String> myList = (prefs.getStringList('mylist') ?? List<String>());
//   // List<dynamic> myOriginaList = myList.map((i) => int.parse(i)).toList();

// print(data);
//  await prefs.setStringList('my_string_list_key', data);
//   // await prefs.setStringList('mylist', data);
// }

class FetchDataJsonPage extends StatefulWidget {
  @override
  _FetchDataJsonPageState createState() => _FetchDataJsonPageState();
}

class _FetchDataJsonPageState extends State<FetchDataJsonPage> {
  Future<List<Post>> posts;

  List data;

  @override
  void initState() {
    super.initState();
    this.getJsonData();
  }

  /// Fetch Data
  ///

  Future<String> getJsonData() async {
    var response = await http
        .get(Uri.encodeFull('https://jsonplaceholder.typicode.com/posts'));

    setState(() {
      var convertDataToJson = json.decode(response.body);
      data = convertDataToJson;
    });
    return "Success";
  }

// Fetch data using class
  Future<List<Post>> getPosts() async {
    final response =
        await http.get('https://jsonplaceholder.typicode.com/posts');

    if (response.statusCode == 200) {
      var parsedPostList = json.decode(response.body);
      List<Post> posts = List();
      parsedPostList.forEach((post) {
        posts.add(Post.fromJson(post));
      });

      return posts;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load ');
    }
  }

// Fetch data from API
  // Future<List<Post>> fetchPosts() async {
  //   // List<Post> posts = [];

  //   var url = 'https://jsonplaceholder.typicode.com/posts';
  //   final response = await http.get(url);
  //   if (response.statusCode == 200) {
  //     setState(() {
  //       var jsonData = json.decode(response.body);
  //       for (var p in jsonData) {
  //         Post post = Post(
  //           userId: p['userId'],
  //           id: p['id'],
  //           title: p['title'],
  //           body: p['body'],
  //         );
  //         // posts.add(post);
  //         postsy.add(post);
  //         // print(postsy);
  //       }
  //     });
  //   }

  //   return postsy;
  // }

  // appBar Style
  final titleStyle = new TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Json Data", style: titleStyle),
          automaticallyImplyLeading: false
        ),
        floatingActionButton: new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.save),
            backgroundColor: new Color(0xFFE57373),
            onPressed: () {
              _saveData(data);
            }),
        body: Container(
            child: ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: Column(
                children: <Widget>[
                  new Card(
                    child: ListTile(
                      title: new Text(data[index]['title']),
                      // padding: const EdgeInsets.all(20.0),
                    ),
                  )
                ],
              ),
            );
          },
        )));
  }
}

// FutureBuilder(
//                 future: getJsonData(),
//                 builder: (BuildContext context, AsyncSnapshot snapshot) {
//                   print(snapshot);
//                   if (snapshot.data == null) {
//                     return Container(
//                       child: Text('Loading...'),
//                     );
//                   } else {
//                     return ListView.builder(
//                       itemCount:
//                           snapshot.data == null ? 0 : snapshot.data.length,
//                       itemBuilder: (BuildContext context, int index) {
//                         return Column(
//                           children: <Widget>[
//                             ListTile(
//                               title: Text(snapshot.data[index].title),
//                             ),
//                           ],
//                         );
//                       },
//                     );
//                   }
//                 })
