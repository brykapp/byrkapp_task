import 'dart:convert';

import 'package:byrapp_task/myApp.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FetchLocalStoragePage extends StatefulWidget {
  @override
  _FetchLocalStoragePageState createState() => _FetchLocalStoragePageState();
}

class _FetchLocalStoragePageState extends State<FetchLocalStoragePage> {
  List data;
  _getList() async {
    final prefs = await SharedPreferences.getInstance();
    final localStorageList = prefs.getString('ListOfData');
    var listOfData = jsonDecode(localStorageList);

    setState(() {
      data = listOfData;
    });
    return data;
  }

  _removeData() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('ListOfData');
  }

  @override
  void initState() {
    super.initState();
    this._getList();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

// appBar Style
  final titleStyle = new TextStyle(
    fontSize: 22.0,
    fontWeight: FontWeight.bold,
  );
  @override
  Widget build(BuildContext context) {
    if (data == null) {
      return Scaffold(
          appBar: AppBar(
            title: Text("Local Storage", style: titleStyle),
          ),
          floatingActionButton: new FloatingActionButton(
              elevation: 0.0,
              child: new Icon(Icons.remove_circle_outline),
              backgroundColor: new Color(0xFFE57373),
              onPressed: () {
                _removeData();
              }),
          body: Center(
              child:  RaisedButton(
              child: Text('Json Page'),
              onPressed: () {
                Navigator.push<bool>(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => MyApp(),
                    ));
              },
            ),
          ));
    } else {
      return Scaffold(
          appBar: AppBar(
            title: Text("Local Storage", style: titleStyle),
          ),
          floatingActionButton: new FloatingActionButton(
              elevation: 0.0,
              child: new Icon(Icons.remove_circle_outline),
              backgroundColor: new Color(0xFFE57373),
              onPressed: () {
                _removeData();
              }),
          body: Container(
              child: ListView.builder(
                      itemCount: data == null ? 0 : data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          child: Column(
                            children: <Widget>[
                              new Card(
                                child: ListTile(
                                  title: new Text(data[index]['title']),
                                  // padding: const EdgeInsets.all(20.0),
                                ),
                              )
                            ],
                          ),
                        );
                      })));
    }
  }

 
}
