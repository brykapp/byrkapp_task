# byrapp_task

## CRUD Flutter App based on `Sqlflite shared_preferences Graphql_Flutter`


## First page (Json Page)

Fetch json data from internet encoded and provide it as a list.
There is a button for saving the data on Local Storage (shared_preferences).

## Second Page (shared_preferences)

It will show the the json data but from local Storage, also there is a button for delete the data from local Storage.

## Third Page (Sqlflite)

It based on `Sqlite database` Full functionality `CRUD`
 

## Fourth Page (Graphql_Flutter)

It based on `graphql database`. 

Change the port to your IP in the `/graphql_operation/port.dart` 

You need to clone the back-end folder 

### git clone https://gitlab.com/brykapp/back-end.git

### npm install && npm start 

### http://localhost:4000/graphql




 Resources:::: 

=[Sqlfilte](https://medium.com/@suragch/simple-sqflite-database-example-in-flutter-e56a5aaa3f91)
- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
